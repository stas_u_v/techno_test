//Подключение библиотек
var gulp = require('gulp'),
    sass = require('gulp-sass'),                    //Подключаем Sass
    autoprefixer = require('gulp-autoprefixer'),    // Подключаем Autoprefixer
    connect = require('gulp-connect-php'),          // Подключаем Connect-php
    del = require('del'),                          // Подключаем Del
    tinypng = require('gulp-tinypng-compress');    //Подключаем Tinypng



//Task scss
gulp.task('scss', function(){
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass({outputStyle: 'extended'}).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 16 versions', 'ie 7', 'ie 8', 'ie 9'],
            cascade: false
        }))
        .pipe(gulp.dest('app/css'))
});

//Task copressed scss
gulp.task('scss_min', function(){
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 16 versions', 'ie 7', 'ie 8', 'ie 9'],
            cascade: false
        }))
        .pipe(gulp.dest('app/css'))
});

// Task connect
gulp.task('connect', function() {
    connect.server({
        base: './app',
        keepalive:true,
        hostname: 'localhost',
        port:8080,
        open: true
    });
});

//Task Watch
gulp.task('watch',['scss','connect'], function() {
    gulp.watch('app/scss/**/*.scss', ['scss']);
});

//Task default
gulp.task('default', ['watch']);

//Task clean
gulp.task('clean', function() {
    return del.sync('dist');
});

// Task TinyPng
gulp.task('tinypng', function () {
    gulp.src('app/img/**/*.{png,jpg,jpeg}')
        .pipe(tinypng({
            key: 'h1iSyh8qlZLBrMuOFZt3lLB1pDiqvft9',
            sigFile: 'images/.tinypng-sigs',
            log: true
        }))
        .pipe(gulp.dest('dist/img'));
});

// Task build
gulp.task('build', ['clean'], function() {

    var buildCss = gulp.src(['app/css/index.css'])
        .pipe(gulp.dest('dist/css'))

    var buildPhp = gulp.src(['app/send.php'])
        .pipe(gulp.dest('dist/'))

    var buildImg = gulp.src('app/img/**/*')
        .pipe(gulp.dest('dist/img'))

    var buildFonts = gulp.src('app/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'))

    var buildJs = gulp.src('app/js/**/*')
        .pipe(gulp.dest('dist/js'))

    var buildHtml = gulp.src('app/*.html')
        .pipe(gulp.dest('dist'));

});
