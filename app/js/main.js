$(document).ready(function () {
    // Hide Login
    $('.auth__call').show();
    $('.login').removeClass('login_active');
    // Call Login
    $('.auth__call').on('click', function () {
        $(this).hide();
        $('.login').addClass('login_active');
    });
    // Close Login
    $('.login__close').on('click', function () {
        $('.login').removeClass('login_active');
        $('.auth__call').show();
    });
    // Remember password
    var remember = $('#remember');
    $('#form').attr('autocomplete', 'off');
    remember.prop('checked', false);
    remember.change(function () {
        if ($(this).is(':checked')) {
            $('#form').attr('autocomplete', 'on');
        }
    });
    // Form validate
    var form = $('#form'),
        button = $(':button[type="submit"]'),
        email = $(':input[type="email"]'),
        password = $(':input[type="password"]');
    // validateEmail
    function validateEmail(emailVal) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/,
            emailList = ["stas@mail.ru", "test@mail.ru", "techno@gmail.com"];

        if (filter.test(emailVal)) {
            if (emailList.indexOf(emailVal) != -1) {
                return true;
            }
        } else {
            return false;
        }
    };
    // validatePassword
    function validatePassword(passwordVal) {
        if (passwordVal.length > 4) {
            return true;
        } else {
            return false;
        }
    }

    // Check Email
    email.bind('change blur click load input', function () {
        var emailVal = email.val();
        $(this).addClass('no-validate');
        if (validateEmail(emailVal)) {
            $(this).removeClass('no-validate').addClass('validate');
        }else {
            $(this).removeClass('validate');
        }
        checkInputs();
    });
    // Check Password
    password.bind('change blur click load input', function () {
        var passwordVal = password.val();
        $(this).addClass('no-validate');
        if (validatePassword(passwordVal)) {
            $(this).removeClass('no-validate').addClass('validate');
        }else{
            $(this).removeClass('validate');
        }
        checkInputs();
    });
    // Check Email and Password
    function checkInputs() {
        if ($('.validate').length == 2) {
            button.prop('disabled', false);
        }else {
            button.prop('disabled', true);
        }
    };
    // Check Email and Password Onload
    function checkInputsOnLoad(callback) {
        var passwordVal = password.val();
        email.addClass('no-validate');
        if (validatePassword(passwordVal)) {
            email.removeClass('no-validate').addClass('validate');
        }else{
            $(this).removeClass('validate');
        }
        var emailVal = email.val();
        password.addClass('no-validate');
        if (validateEmail(emailVal)) {
            password.removeClass('no-validate').addClass('validate');
        }else{
            $(this).removeClass('validate');
        }
        callback();
    }
    checkInputsOnLoad(function () {
        checkInputs();
    });

    // Add disabled on button
    button.prop('disabled', true);
    // Submit Form
    form.on('submit', function (e) {
        e.preventDefault();
        $.post({
            url: 'send.php',
            type: 'post',
            data: $(this).serialize(),
            beforeSend: function () {
                button.attr('disableb','disabled');
                email.attr('disableb','disabled');
            },
            success: function (post) {
                button.removeAttr('disableb');
                email.removeAttr('disableb');
                $('.login').removeClass('login_active');
                $('.auth').append().text(post);
            }
        });
    });
});
